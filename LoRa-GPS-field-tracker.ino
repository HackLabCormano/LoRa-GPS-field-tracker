/**
 * LoRa GPS field tracker
 *
 * by atrent (http://atrent.it)
 *
 * AGPLv3
 *
 */

/* BOARD: LOLIN Wemos D1 & R2 mini
 * https://wiki.wemos.cc/products:d1:d1_mini
 *
 * PIN map (9 in totale portati fuori sulla board)
 * LoRa module (2pin) Wemos D8=15 e D0=16, da verificare funzionamento
 * TM1638 (3pin)
 * 
 * GPS (1 pin, solo rx, ma swserial ne occupa 2)
 * http://www.etechpath.com/mini-gps-display-using-ublox-neo-6m-module-and-esp8266-nodemcu/
 * plummino andava? (cfr)
 * usare tx/rx della seriale di programmazione? funziona?
 * abbandonare swserial?
 * forse Serial1 è attaccata a TX/RX?
 * alimentazione? troppo 5V?
 *
 * RST		TX
 * A0		RX
 * D0-l		D1-g
 * D5-t		D2-g
 * D6-t		D3
 * D7-t		D4
 * D8-l		GND
 * 3V3		5V
 *
 * TODO "moduli" da inserire
 * - gps (neoswserial e tinygpsplus? o uso la seriale normale? tanto poi solo OTA. attenzione che ESP ha una sua swserial apposta)
 * - TM1638 (menù?)
 *		per esp8266+tm1638
 *		usare TM1638lite tm(D2, D1, D0); FUNZIONA esempio, confligge con LMIC shield, TODO cambiare pins
 *		cfr. anche https://github.com/pasz1972/HassMqqtAlarmPanelTM1638
 * - LoRa LMIC (Brianza) per ultimo
 * - (espwebconfig?)
 * - softAP? per avere info collegandosi col cell al wifi del ESP
 *
 * TODO app ttn
 * - copiare appkey
 * - preparare integrazione json -> ttnmapper
 *
 * TODO sostituire tutti i println...? (solo se non va il swserial)
 *
 * TM1638 schema funzioni
 * LED:		[SND] [GPS] [...] [AP ] [...] [...] [...] [ATN]
 * 7SEG:		(scorrevole/variabile/contestuale)
 * BTN:		[SND] [...] [...] [AP ] [ESC] [<<<] [>>>] [OK]
 */

/////////////////////////////////////////////////
// Lib

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <TaskScheduler.h>
#include <ArduinoOTA.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <TM1638lite.h>

/////////////////////////////////////////////////
// Globals

#define APSSID "LoRa"
#define APPSK  "_lora999"
/*
IPAddress local_IP(192,168,4,77);
IPAddress gateway(192,168,4,1);
IPAddress subnet(255,255,255,0);
*/

// TaskScheduler
Scheduler runner;

// TM1638
// I/O pins on the Arduino connected to strobe, clock, data
// (power should go to 3.3v and GND)
TM1638lite tm(D5, D6, D7);

// GPS e swserial
TinyGPSPlus gps;
SoftwareSerial sws(D1, D2);

/////////////////////////////////////////////////
void loop() {
    runner.execute(); // TaskScheduler
    ArduinoOTA.handle(); // OTA update
}

/////////////////////////////////////////////////
void setup_OTA() {
    Serial.println("/OTA...");

    // Port defaults to 8266
    ArduinoOTA.setPort(8266);

    // Hostname defaults to esp8266-[ChipID]
    ArduinoOTA.setHostname("LoRaTracker");

    // No authentication by default
    ArduinoOTA.setPassword((const char *)"LoRa");

    ArduinoOTA.onStart([]() {
        Serial.println("OTA Start"); // TODO meglio lampeggio led
    });

    ArduinoOTA.onEnd([]() {
        Serial.println("OTA End"); // TODO meglio lampeggio led
    });

    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("OTA Progress: %u%%\r", (progress / (total / 100))); // TODO meglio lampeggio led
    });

    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("OTA Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
        else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
        else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
        else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
        else if (error == OTA_END_ERROR) Serial.println("End Failed");

        // TODO meglio lampeggio led
    });

    ArduinoOTA.begin();

    Serial.println("OTA started/");
}

/////////////////////////////////////////////////
void setup_GPS() {
    Serial.println("/GPS...");
    sws.begin(9600);
    Serial.println(F("DeviceExample.ino"));
    Serial.println(F("A simple demonstration of TinyGPS++ with an attached GPS module"));
    Serial.print(F("Testing TinyGPS++ library v. "));
    Serial.println(TinyGPSPlus::libraryVersion());
    Serial.println(F("by Mikal Hart"));
    Serial.println("GPS started/");
}

/////////////////////////////////////////////////
void displayGps() {
    Serial.print(F("Location: "));
    if (gps.location.isValid()) {
        Serial.print(gps.location.lat(), 6);
        Serial.print(F(","));
        Serial.print(gps.location.lng(), 6);
    } else {
        Serial.print(F("INVALID"));
    }

    Serial.print(F("  Date/Time: "));
    if (gps.date.isValid()) {
        Serial.print(gps.date.month());
        Serial.print(F("/"));
        Serial.print(gps.date.day());
        Serial.print(F("/"));
        Serial.print(gps.date.year());
    } else {
        Serial.print(F("INVALID"));
    }

    Serial.print(F(" "));
    if (gps.time.isValid()) {
        if (gps.time.hour() < 10) Serial.print(F("0"));
        Serial.print(gps.time.hour());
        Serial.print(F(":"));
        if (gps.time.minute() < 10) Serial.print(F("0"));
        Serial.print(gps.time.minute());
        Serial.print(F(":"));
        if (gps.time.second() < 10) Serial.print(F("0"));
        Serial.print(gps.time.second());
        Serial.print(F("."));
        if (gps.time.centisecond() < 10) Serial.print(F("0"));
        Serial.print(gps.time.centisecond());
    } else {
        Serial.print(F("INVALID"));
    }

    Serial.println();
}

/////////////////////////////////////////////////
// Tasks

/* legge GPS */
Task taskGPS(50*TASK_MILLISECOND, TASK_FOREVER, []() {
    // prova con serial normale
    if(Serial.available()>0)
        Serial.print(Serial.read());

    // This sketch displays information every time a new sentence is correctly encoded.
    while (sws.available() > 0) {
        char c=sws.read();
        if (gps.encode(c))
            displayGps();
    }
    /*
    if (millis() > 5000 && gps.charsProcessed() < 10) {
        Serial.println(F("No GPS detected: check wiring."));
        while(true);
    }
    */
});

/* invia coordinate */
Task taskBeacon(15*TASK_SECOND, TASK_FOREVER, []() {
    Serial.println("beacon");
});

/* status su display e led */
Task taskStatus(TASK_SECOND, TASK_FOREVER, []() {
    Serial.println("status");
});

/* legge bottoni e scatena azioni */
Task taskButtons(100*TASK_MILLISECOND, TASK_FOREVER, []() {
    //Serial.println("buttons");
    uint8_t buttons = tm.readButtons();
    doLEDs(buttons);
});

/////////////////////////////////////////////////
void setup() {
    Serial.begin(9600);
    Serial.println("booting...");

    // vari setup
    setup_WIFI();
    setup_OTA();
    setup_GPS();
    setup_TM();

    // Tasks
    runner.init();

    runner.addTask(taskGPS);
    taskGPS.enable();

    /*
    runner.addTask(taskBeacon);
    taskBeacon.enable();

    runner.addTask(taskStatus);
    taskStatus.enable();
    */

    runner.addTask(taskButtons);
    taskButtons.enable();

    Serial.println("booted!");
}

/////////////////////////////////////////////////
void setup_TM() {
    Serial.println("/TM...");

    tm.reset();

    tm.displayText("Eh");
    tm.setLED(0, 1);

    delay(1000);

    tm.displayASCII(6, 'u');
    tm.displayASCII(7, 'p');
    tm.setLED(7, 1);

    delay(1000);

    tm.displayHex(0, 8);
    tm.displayHex(1, 9);
    tm.displayHex(2, 10);
    tm.displayHex(3, 11);
    tm.displayHex(4, 12);
    tm.displayHex(5, 13);
    tm.displayHex(6, 14);
    tm.displayHex(7, 15);

    delay(1000);

    tm.displayText("buttons");

    Serial.println("TM started/");
}

// scans the individual bits of value
void doLEDs(uint8_t value) {
    for (uint8_t position = 0; position < 8; position++) {
        tm.setLED(position, value & 1);
        value = value >> 1;
    }
}

void setup_WIFI() {
    //delay(1000);
    Serial.print("/Configuring access point...");
    WiFi.softAP(APSSID, APPSK);
    //Serial.println(WiFi.softAPConfig(local_IP, gateway, subnet) ? "Ready" : "Failed!");
    IPAddress myIP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(myIP);
    Serial.println("AP started/");
}
